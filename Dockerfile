From tomcat:8.5.32

run apt-get update -y
run apt-get upgrade -y
cp target/DateTimeWebapp-1.0.war /usr/local/tomcat/webapps/app.war

expose 8080

cmd ["catalina.sh", "run"]
